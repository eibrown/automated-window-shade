# Welcome

Welcome to our Automated Window Shade project for our ECE 583 Class.  We will be posting a list of all of our equipment, wiring diagrams, and the source code.

## Our Project

### Getting Started

For our project, we utilized tutorials found on the Adafruit website to help us control our DC motor, which we then modified to use with our window shade project.


#### Materials Needed

* Raspberry Pi

![learn_raspberry_pi_pi.jpg](https://bitbucket.org/repo/5MGqg8/images/632879894-learn_raspberry_pi_pi.jpg =200x)

* Adafruit Cobbler Breakout Board

![learn_raspberry_pi_cobbler.jpg](https://bitbucket.org/repo/5MGqg8/images/2072810327-learn_raspberry_pi_cobbler.jpg)

* Male Jumper Leads

![learn_raspberry_pi_jumpers_web.jpg](https://bitbucket.org/repo/5MGqg8/images/948657746-learn_raspberry_pi_jumpers_web.jpg)

* Breadboard

![learn_raspberry_pi_breadboard_half_web.jpg](https://bitbucket.org/repo/5MGqg8/images/3975191000-learn_raspberry_pi_breadboard_half_web.jpg)

* DC Motor

![learn_raspberry_pi_motor.jpg](https://bitbucket.org/repo/5MGqg8/images/2005331227-learn_raspberry_pi_motor.jpg)

* L293D Motor Controller IC

![learn_raspberry_pi_L293d.jpg](https://bitbucket.org/repo/5MGqg8/images/54501866-learn_raspberry_pi_L293d.jpg)

* Power Source

#### PWM

Pulse Width Modulation (or PWM) is a technique for controlling power. We use it here to control the amount of power going to the motor and hence how fast it spins.

The diagram below shows the signal from the PWM pin of the Raspberry Pi.

![learn_raspberry_pi_how_pwm_works.jpg](https://bitbucket.org/repo/5MGqg8/images/1095000377-learn_raspberry_pi_how_pwm_works.jpg)

Every 1/500 of a second, the PWM output will produce a pulse. The length of this pulse controls the amount of energy that the motor gets. No pulse at all and the motor will not turn, a short pulse and it will turn slowly. If the pulse is active for half the time, then the motor will receive half the power it would if the pulse stayed high until the next pulse came along.

